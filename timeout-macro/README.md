# timeout macro

A small proc macro helper that runs test-cases in a timeout.

```
    #[test(timeouttest)]
    async fn slow_test() {
        let dur = Duration::from_secs(200);
        tokio.time.sleep(dur).await
    }
```
