# Getting sqlx stuff ready

Either 

$ export SQLX_OFFLINE=true

to use the cached queries when compiling insted of rebuilding them.
Only use this when nothing is changed with queries or databas schemas.

Or do the following setup

Setup dev environment (one-time):

$ cargo install sqlx-cli

In top level, run:

$ cargo sqlx database setup --source loggerdb/migrations
$ cargo sqlx prepare --merged
