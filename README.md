# Modio logger

This is a reimplementation of the old Modio Logger in rust, with the following goals:

- Implement 1:1 the old fsipc based interface
- Implement the new ipc interface with better support for units etc.
- Implement support for old databases
- Implement support for new database schema
- Include support for db migrations


# Functionality

The basic functionality of the logger is to act as a frontend of the on-device
database and the various logger services.

This may seem like a simple thing, but it turns out to have a fair bit of
subtletly involved.

# Persistence

- All collectors should be able to restart and come back on with the
  expectation the the modio logger has kept their data around
- If the submitter fails to submit and crashes/restarts, the logger should be
  able to let it re-submit data
- After data has been submitted, the last value of every sensor should be
  available to local system services
- When the disk space approaches or becomes full (for example, if the device is
  offline for a longer time), some elected data should be removed in order to
  save space
- Multiple submitters for multiple namespaces (modio, customer) should be able
  to get data out of the logger and mark it for deletion
- The logger should reduce the amount of disk-writes if possible to make sure
  the flash last longer
- Transactions should not be repeated
