## mocklogger

This implements a modio-logger dbus-service on the Session bus, mostly to be
able to run complete integration tests in other projects.

if you do not have a dbus-session (container, CI, etc) launch under:

$ dbus-run-session  -- cargo run

And then test that things work as intended
