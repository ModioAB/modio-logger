// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
pub const LOGGER_NAME: &str = "se.modio.logger";
pub const LOGGER_PATH: &str = "/se/modio/logger";

pub mod conn;
pub mod logger;
pub mod logger1;
pub mod sd_notify;
pub mod submit1;
#[cfg(test)]
pub mod testing;
pub mod timefail;
pub(crate) mod types;
pub(crate) mod values;
