// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use async_std::task;

/// Creates an connection to the bus, and spawns a task to poll it's connection.
pub async fn make_connection(session: bool) -> zbus::Result<zbus::Connection> {
    let build = if session {
        zbus::ConnectionBuilder::session()?
    } else {
        zbus::ConnectionBuilder::system()?
    };

    let conn = build.internal_executor(false).build().await?;
    let conn_clone = conn.clone();
    // Set up a tick-loop for the connections executor
    // Otherwise it spawns one on it's own (internal_executor = true)
    // or deadlocks (internal_executor= false)
    task::spawn(async move {
        loop {
            conn_clone.executor().tick().await;
        }
    });
    Ok(conn)
}
