// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use std::path::Path;
use std::sync::Arc;

use fsipc::legacy::fsipcProxy;

use super::logger::{Logger, LoggerPing};
use super::logger1::Logger1;
use super::submit1::Submit1;
use super::timefail::Timefail;
use super::LOGGER_PATH;
use crate::conn::make_connection;
use crate::logger::LogErr;
use modio_logger_db::Datastore;
use tracing::{debug, info};

use async_std::task;
use tempfile;
use tempfile::NamedTempFile;

#[derive(Debug, Clone)]
pub struct Tempbase {
    dir: Arc<tempfile::TempDir>,
}

impl Tempbase {
    #[must_use]
    pub fn new() -> Self {
        let dir = tempfile::Builder::new()
            .prefix("modio-logger")
            .tempdir()
            .expect("Error tempdir");

        Self { dir: dir.into() }
    }

    #[must_use]
    /// Create a new dbfile (Temp) and return it. NOT rooted in the same as the base, so keep a
    /// handle on it.
    pub fn dbfile() -> Arc<NamedTempFile> {
        let dbfile = tempfile::Builder::new()
            .prefix("modio-logger-database")
            .suffix(".sqlite")
            .tempfile()
            .expect("Error on tempfile");
        dbfile.into()
    }

    #[must_use]
    pub fn timeadjust_path(&self) -> Box<Path> {
        self.dir.path().join("timeadjust").into_boxed_path()
    }

    #[must_use]
    pub fn timefail_path(&self) -> Box<Path> {
        self.dir.path().join("timefail").into_boxed_path()
    }
}

impl Default for Tempbase {
    fn default() -> Self {
        Self::new()
    }
}

pub(crate) mod proxytest {

    pub use fsipc::logger1::Metadata;
    pub use fsipc::logger1::SensorMode;
    pub use fsipc::logger1::ValueMap;
    use std::collections::HashMap;
    use zbus::proxy;
    use zbus::zvariant::Value;

    #[proxy(
        interface = "se.modio.logger.Logger1",
        default_service = "se.modio.logger",
        default_path = "/se/modio/logger"
    )]
    trait Logger1 {
        fn get_metadata(&self, key: &str) -> zbus::Result<Metadata>;
        fn set_metadata_description(&self, key: &str, description: &str) -> zbus::Result<()>;
        fn set_metadata_mode(&self, key: &str, mode: SensorMode) -> zbus::Result<()>;
        fn set_metadata_name(&self, key: &str, name: &str) -> zbus::Result<()>;
        fn set_metadata_unit(&self, key: &str, unit: &str) -> zbus::Result<()>;
        fn set_metadata_row(&self, key: &str, row_items: Vec<(&str, &str)>) -> zbus::Result<()>;
        fn set_metadata_value_map(&self, key: &str, value_map: ValueMap) -> zbus::Result<()>;
        #[zbus(signal)]
        fn metadata_updated(&self, key: &str) -> zbus::Result<()>;
        async fn store_batch(&self, batch: HashMap<String, Value<'_>>) -> zbus::Result<()>;
        async fn store(&self, key: String, value: Value<'_>) -> zbus::Result<()>;
        async fn store_rows(&self, key: &str, rows: Vec<(f64, Vec<Value<'_>>)>)
            -> zbus::Result<()>;
        async fn store_ts(&self, batch: Vec<(String, Value<'_>, f64)>) -> zbus::Result<()>;
        #[zbus(signal)]
        async fn store_signal(&self, batch: Vec<(String, Value<'_>, i64)>) -> zbus::Result<()>;
    }

    #[proxy(
        interface = "se.modio.logger.Submit1",
        default_service = "se.modio.logger"
    )]
    trait Submit1 {
        async fn get_all_metadata(&self) -> zbus::Result<Vec<Metadata>>;
    }
}

pub(crate) struct TestServer {
    name: String,
    pub conn: zbus::Connection,
    drop_event: Arc<tokio::sync::Notify>,
}

impl TestServer {
    pub async fn new(line: u32) -> Result<Self, LogErr> {
        let name = format!("se.modio.logger.TestCase{line}");
        let base = Tempbase::default();
        let dbfile = Tempbase::dbfile();
        Self::new_with_base(name, base, dbfile).await
    }

    pub async fn new_with_base(
        name: String,
        base: Tempbase,
        dbfile: Arc<NamedTempFile>,
    ) -> Result<Self, LogErr> {
        let timefail = Timefail::new(base.timefail_path(), base.timeadjust_path());
        debug!(target: "TestServer","Setting up DB pool in {} with dbfile {}", base.dir.path().display(), dbfile.path().display());
        let ds = Datastore::new_tempfile(dbfile).await;
        let logger = Logger::new(timefail.clone(), ds.clone()).await?;
        let logger_ping = LoggerPing {};
        let submit1 = Submit1::new(timefail.clone(), ds.clone(), true).await?;
        let logger1 = Logger1::new(timefail, ds).await?;

        let conn = make_connection(true).await?;
        conn.object_server().at(LOGGER_PATH, logger).await?;
        conn.object_server().at(LOGGER_PATH, logger_ping).await?;
        conn.object_server().at(LOGGER_PATH, logger1).await?;
        conn.object_server().at(LOGGER_PATH, submit1).await?;

        debug!(target: "TestServer", "Requesting name {name} for the connection");
        conn.request_name(name.clone()).await?;

        let drop_event = Self::drop_event(name.clone(), conn.clone(), base);

        let res = Self {
            name,
            conn,
            drop_event,
        };
        Ok(res)
    }

    pub async fn proxy(&self) -> Result<fsipcProxy<'static>, LogErr> {
        debug!(target: "TestServer", "Connecting fsipc proxy to {}", &self.name);
        let proxy = fsipcProxy::builder(&self.conn)
            .path(LOGGER_PATH)?
            .destination(self.name.clone())?
            .build()
            .await?;
        // Check that the server is alive before we hand it over to test-cases.
        proxy.ping().await?;
        Ok(proxy)
    }
    //pub async fn logger1(&self) -> Result<fsipc::logger1::Logger1Proxy<'static>, LogErr> {
    pub async fn logger1(&self) -> Result<proxytest::Logger1Proxy<'static>, LogErr> {
        debug!(target: "TestServer", "Connecting Logger1 proxy to {}", &self.name);
        //let proxy = fsipc::logger1::Logger1Proxy::builder(&self.conn)
        let proxy = proxytest::Logger1Proxy::builder(&self.conn)
            .path(LOGGER_PATH)?
            .destination(self.name.clone())?
            .build()
            .await?;
        Ok(proxy)
    }
    pub async fn submit1(&self) -> Result<proxytest::Submit1Proxy<'static>, LogErr> {
        debug!(target: "TestServer", "Connecting Submit1 proxy to {}", &self.name);
        let proxy = proxytest::Submit1Proxy::builder(&self.conn)
            .path(LOGGER_PATH)?
            .destination(self.name.clone())?
            .build()
            .await?;
        Ok(proxy)
    }

    fn drop_event(
        name: String,
        conn: zbus::Connection,
        base: Tempbase,
    ) -> Arc<tokio::sync::Notify> {
        let notify = Arc::new(tokio::sync::Notify::new());

        let waiting = notify.clone();

        task::spawn(async move {
            waiting.notified().await;
            info!(target:"TestServer::drop_event", "Releasing name from bus {name}");
            conn.release_name(name.clone())
                .await
                .expect("Failed to release name");

            // Release the loggers to make them drop their resources, like the database.
            // Otherwise they will linger as long as there is a connection object kept alive, which
            // may be a few as it is clone() able
            //
            // Call once per type of interface.  legacy::logger
            let iface = conn
                .object_server()
                .remove::<Logger, _>(LOGGER_PATH)
                .await
                .expect("Failed to release Logger");
            debug!(target:"TestServer::drop_event", "Released Logger interface at {LOGGER_PATH} res={iface}");

            // Logger1
            let iface = conn
                .object_server()
                .remove::<Logger1, _>(LOGGER_PATH)
                .await
                .expect("Failed to release Logger1");
            debug!(target:"TestServer::drop_event", "Released Logger1 interface at {LOGGER_PATH} res={iface}");

            // And LoggerPing just for completion
            let iface = conn
                .object_server()
                .remove::<LoggerPing, _>(LOGGER_PATH)
                .await
                .expect("Failed to release LoggerPing");
            debug!(target:"TestServer::drop_event", "Released LoggerPing interface at {LOGGER_PATH} res={iface}");

            info!(target:"TestServer::drop_event", "Finished cleaning out after us.");
            drop(conn);
            info!(target:"TestServer::drop_event", "Paths left: {:?}", base);
        });
        notify
    }
}

impl Drop for TestServer {
    fn drop(&mut self) {
        // As drop is always called in a SYNC context, and it is hairy to recurse and find an async
        // executor that would allow us to write buffered data to disk, instead we have a task
        // waiting when creating the Datastore, which we notify here. That task will then run the
        // job of writing data to disk and closing files properly.
        self.drop_event.notify_waiters();
        // TODO: Maybe we should wait for the task to be complete as well?
    }
}
