// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fmt;
pub type ValueMap = HashMap<u32, String>;
use fsipc::logger1::SensorMode;
type Name = String;
type Description = String;
//type Token = String;

const SENML_RECOMMENDED_UNITS: [&str; 64] = [
    // 2022-01: Spindel
    // It's important that this list matches the submit server list that is kept in:
    // https://gitlab.com/ModioAB/submit/-/blob/master/src/submit/units.py
    //
    // Units from RFC 8428, excluding the not recommended ones:
    "m", "kg", "s", "A", "K", "cd", "mol", "Hz", "rad", "sr", "N", "Pa", "J", "W", "C", "V", "F",
    "Ohm", "S", "Wb", "T", "H", "Cel", "lm", "lx", "Bq", "Gy", "Sv", "kat", "m2", "m3", "m/s",
    "m/s2", "m3/s", "W/m2", "cd/m2", "bit", "bit/s", "lat", "lon", "pH", "dB", "dBW", "count", "/",
    "%RH", "%EL", "EL", "1/s", "beats", "S/m",
    //Units from draft-ietf-core-senml-more-units-06 excluding the
    //secondary and not recommended ones:
    "B", "VA", "VAs", "var", "vars", "J/m", "kg/m3",
    //
    // Fractions we explicitly support:
    "/10", "/100", "/1000", "/100000", "/1000000",
    // Secondary units we accept begrudningly due to joule conversion causing large precision loss.
    "kWh",
];

// 2022-01:
// Const sets aren't stable yet, otherwise the above array would be much better as a Set.
// https://github.com/rust-lang/rust/issues/71835
//
// use std::collections::BTreeSet;
// const SENML_UNITS: BTreeSet<&str> = BTreeSet::from(SENML_RECOMMENDED_UNITS);

#[derive(Debug, PartialEq, Type, Serialize, Deserialize)]
#[serde(try_from = "String")]
pub(crate) struct Unit(String);

// Invalid unit
#[derive(thiserror::Error, Debug)]
pub enum UnitError {
    #[error("Unknown unit")]
    Unknown,
    #[error("May not replace unit")]
    Unique,
}

// Invalid DataType
#[derive(thiserror::Error, Debug)]
pub enum DataTypeError {
    #[error("Unknown data type")]
    Unknown,
    #[error("May not replace data_type")]
    Unique,
    #[error("Value count mismatch")]
    RowCount,
    #[error("Serialization failed")]
    Serde(#[from] serde_json::Error),
}

// Try to convert an arbitary string to a Unit string
impl TryFrom<String> for Unit {
    type Error = UnitError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Self::try_new(value)
    }
}

impl TryFrom<&String> for Unit {
    type Error = UnitError;

    fn try_from(value: &String) -> Result<Self, Self::Error> {
        Self::try_new(value)
    }
}

impl Unit {
    /// Try to make a new Unit, by checking the Value
    fn try_new(input: impl AsRef<str>) -> Result<Self, UnitError> {
        if SENML_RECOMMENDED_UNITS.iter().any(|v| *v == input.as_ref()) {
            Ok(Self(input.as_ref().into()))
        } else {
            Err(UnitError::Unknown)
        }
    }

    /// Helper, makes for neater unit-tests
    #[allow(dead_code)]
    fn from_str(input: &str) -> Option<Self> {
        // this should do try_new and return None if failure
        Self::try_new(input).ok()
    }

    /// Consume self and become String
    #[allow(dead_code)]
    pub fn into_inner(self) -> String {
        self.0
    }
    /// Peek at the inside
    #[allow(dead_code)]
    pub const fn inner(&self) -> &String {
        &self.0
    }

    #[cfg(test)]
    /// Convert in-str to String and validate it.
    pub fn string(input: &str) -> String {
        Self::try_new(input).expect("Invalid unit").into_inner()
    }
}

impl fmt::Display for Unit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.0)
    }
}

#[derive(Debug, PartialEq, Type, Serialize, Deserialize)]
enum Value {
    String,
    Bool,
    Int,
    Float,
}

use zbus::zvariant::{DeserializeDict, SerializeDict, Type};
#[derive(Debug, PartialEq, DeserializeDict, SerializeDict, Type)]
#[zvariant(signature = "dict")]
pub(crate) struct Metadata {
    pub(crate) n: String,
    pub(crate) u: Option<Unit>,
    pub(crate) name: Option<Name>,
    pub(crate) description: Option<Description>,
    pub(crate) mode: Option<SensorMode>,
    pub(crate) value_map: Option<ValueMap>,
}

impl From<modio_logger_db::Metadata> for Metadata {
    fn from(other: modio_logger_db::Metadata) -> Self {
        // other.u is Option<String>
        // This turns it into Option<Unit> by using map_or  with default None
        let unit: Option<Unit> = other.u.and_then(|x| Unit::try_new(x).ok());
        Self {
            n: other.n,
            u: unit,
            name: other.name,
            description: other.description,
            mode: other.mode.map(std::convert::Into::into),
            value_map: other.value_map,
        }
    }
}

#[cfg(test)]
mod test {
    use super::Unit;
    use std::error::Error;

    #[test]
    fn can_make_unit() -> Result<(), Box<dyn Error>> {
        Unit::try_new("m")?;
        Unit::try_new("Cel")?;
        Unit::try_new("/")?;
        Unit::try_new("/100")?;
        Unit::try_new("kWh")?;
        Unit::try_new("KWh").unwrap_err();
        Unit::try_new("humbug").unwrap_err();
        Ok(())
    }

    #[test]
    fn can_print_unit() -> Result<(), Box<dyn Error>> {
        let m = Unit::try_new("m")?;
        let res = format!("{m}");
        assert_eq!(res, "m");
        Ok(())
    }

    #[test]
    fn invalid_celsius() {
        Unit::from_str("m").unwrap();
        Unit::from_str("Cel").unwrap();
        assert_eq!(Unit::string("Cel"), "Cel".to_string());
        assert!(Unit::from_str("Celsius").is_none());
    }
}
