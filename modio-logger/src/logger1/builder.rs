// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use crate::logger::LogErr;
use crate::timefail;
use modio_logger_db::{Datastore, SqlitePool};

use super::Logger1;

pub struct Builder {
    ds: Option<Datastore>,
    development: bool,
}
impl Builder {
    /// New builder
    #[must_use]
    pub const fn new() -> Self {
        Self {
            ds: None,
            development: false,
        }
    }
    /// Set the datastore
    ///
    /// If this is not called, it will use a temporary datastore
    #[must_use]
    pub fn datastore(mut self, ds: Datastore) -> Self {
        self.ds = Some(ds);
        self
    }

    /// Set the datastore from a pool
    ///
    /// Replaces the datastore with one based from the pool
    pub async fn datastore_pool(mut self, p: &SqlitePool) -> Self {
        let pool = p.clone();
        let ds = Datastore::new(pool)
            .await
            .expect("Failed to make datastore");
        self.ds = Some(ds);
        self
    }

    /// Set development mode to true/false
    ///
    /// default is false
    #[must_use]
    pub const fn development(mut self, val: bool) -> Self {
        self.development = val;
        self
    }

    pub async fn build(self) -> Result<Logger1, LogErr> {
        let tf = if self.development {
            timefail::Timefail::development()
        } else {
            timefail::Timefail::default()
        };

        let datastore = if let Some(ds) = self.ds {
            ds
        } else {
            Datastore::temporary().await
        };
        let logger1 = Logger1::new(tf, datastore).await?;
        Ok(logger1)
    }
}

impl Default for Builder {
    fn default() -> Self {
        Self::new()
    }
}
