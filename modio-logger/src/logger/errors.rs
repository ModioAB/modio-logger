// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use super::keys;
use crate::types::DataTypeError;
use crate::types::UnitError;
use zbus::DBusError;

#[derive(Debug, DBusError)]
#[zbus(prefix = "se.modio.logger.fsipc")]
pub enum LogErr {
    #[zbus(error)]
    ZBus(zbus::Error),
    KeyError(String),
    NotFound,
    NoMetadata,
    TransactionNotFound,
    InvalidPrepared(String),
    Datastore(String),
    Buffer(String),
    IOError(String),
    UnitError(String),
    DataTypeError(String),
    Serde(String),
}

impl From<std::io::Error> for LogErr {
    fn from(e: std::io::Error) -> Self {
        let msg = e.to_string();
        Self::IOError(msg)
    }
}

impl From<modio_logger_db::Error> for LogErr {
    fn from(e: modio_logger_db::Error) -> Self {
        match e {
            modio_logger_db::Error::NotFound { .. } => Self::NotFound,
            _ => {
                let msg = e.to_string();
                Self::Datastore(msg)
            }
        }
    }
}

impl From<serde_json::Error> for LogErr {
    fn from(e: serde_json::Error) -> Self {
        let msg = e.to_string();
        Self::Serde(msg)
    }
}

impl From<keys::KeyError> for LogErr {
    fn from(e: keys::KeyError) -> Self {
        let msg = e.to_string();
        Self::KeyError(msg)
    }
}

impl From<UnitError> for LogErr {
    fn from(e: UnitError) -> Self {
        let msg = e.to_string();
        Self::UnitError(msg)
    }
}

impl From<DataTypeError> for LogErr {
    fn from(e: DataTypeError) -> Self {
        let msg = e.to_string();
        Self::DataTypeError(msg)
    }
}
