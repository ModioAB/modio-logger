// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use zbus::interface;

pub struct LoggerPing {}

#[interface(name = "se.modio.logger")]
impl LoggerPing {
    #[allow(clippy::unused_self)]
    const fn ping(&self) -> &str {
        "Ping pong"
    }
}
