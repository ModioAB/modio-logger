// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use argh::FromArgs;
use async_std::task::sleep;
use mocklogger::conn::make_connection;
use mocklogger::logger::{Logger, LoggerPing};
use mocklogger::logger1::Logger1;
use mocklogger::sd_notify;
use mocklogger::submit1::Submit1;
use mocklogger::timefail;
use mocklogger::LOGGER_NAME;
use mocklogger::LOGGER_PATH;
use modio_logger_db::{Datastore, SqlitePool, SqlitePoolBuilder};
use std::error::Error;
use std::path::PathBuf;
use std::time::{Duration, Instant};
use tracing::{error, info, warn};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

const VERSION: &str = env!("CARGO_PKG_VERSION");

/// The modio-logger
///
/// This is the modio-logger, replacing the python version.
/// It will use databases in /modio/mytemp.sqlite  or development/mytemp.sqlite
#[derive(Debug, FromArgs)]
struct Config {
    /// use session bus
    ///
    /// By default we use the DBus System bus, add this flag to use the Session bus, or "user" bus
    /// instead. Mainly useful when developing.
    #[argh(switch)]
    session: bool,

    /// verbosity
    ///
    /// Add more -v -v -v in order to get a higher log level.
    #[argh(switch, short = 'v')]
    verbose: u32,

    /// db_connections
    ///
    /// The amount of db connections to set up.
    #[argh(option)]
    db_connections: Option<u32>,

    /// db_timeout
    ///
    /// The timeout of the db in seconds
    #[argh(option)]
    db_timeout: Option<u32>,
}

async fn make_pool(
    session: bool,
    db_connections: Option<u32>,
    db_timeout: Option<u32>,
) -> Result<SqlitePool, Box<dyn Error>> {
    use async_std::fs::File;

    let database_path = if session {
        PathBuf::from("development/mytemp.sqlite")
    } else {
        PathBuf::from("/modio/mytemp.sqlite")
    };

    if !database_path.exists() {
        warn!("DB does not exist. creating {:?}", database_path);
        let _file = File::create(&database_path).await?;
    }
    let pool = SqlitePoolBuilder::new()
        .db_path(&database_path)
        .migrate(true)
        .db_connections(db_connections)
        .db_timeout(db_timeout)
        .build()
        .await?;
    Ok(pool)
}

// Match a verbose flag to one of our given default-ish filters.
// This is equivalent to our previous env_logger based logic in order to not change the default
// behaviour too much.
const fn verbosity(verbose: u32) -> &'static str {
    match verbose {
        0 => "error",
        1 => "error,mocklogger=warn,modio_logger_db=warn,modio_logger=warn,sqlx::query=warn",
        2 => "error,mocklogger=info,modio_logger_db=info,modio_logger=info,sqlx::query=warn",
        3 => "error,mocklogger=debug,modio_logger_db=debug,modio_logger=debug,sqlx::query=warn",
        4 => "warn,mocklogger=trace,modio_logger_db=trace,modio_logger=trace,sqlx::query=info",
        5 => "info,mocklogger=trace,modio_logger_db=trace,modio_logger=trace,sqlx::query=debug",
        6 => "debug,mocklogger=trace,modio_logger_db=trace,modio_logger=trace,sqlx::query=trace",
        _ => "trace",
    }
}

#[test]
fn check_verbosity() -> Result<(), Box<dyn Error>> {
    // Check that our verbosity strings parse so we don't get an error at runtime
    for x in 0..9 {
        let filt = verbosity(x);
        EnvFilter::builder().parse(filt)?;
    }
    Ok(())
}

// Iff we have the RUST_LOG env-var set and it is valid, use that.
// Otherwise, use a built-in default based on the -v flags.
#[allow(clippy::option_if_let_else)]
fn get_log_filter(verbose: u32) -> EnvFilter {
    let default_directive = verbosity(verbose);
    let env_filt = EnvFilter::builder().try_from_env();
    if let Ok(filter) = env_filt {
        filter
    } else {
        EnvFilter::builder().parse_lossy(default_directive)
    }
}

// Helper to calculate a suitable delay
fn periodic_delay() -> Duration {
    let restful_prime = Duration::from_millis(29959);
    // use half the value from env, or restful_prime if we could not parse it.
    let delay = sd_notify::sd_watchdog_usec().map_or(restful_prime, |dur| dur / 2);
    // Cap the delay to at least the default
    delay.min(restful_prime)
}

// Hacky little timer to wait until a certain moment
struct Period {
    start: Instant,
    wait: Duration,
}
impl Period {
    fn start(wait: Duration) -> Self {
        Self {
            start: Instant::now(),
            wait,
        }
    }
    async fn sleep(&self) {
        let spent = self.start.elapsed();
        let remains = self.wait.saturating_sub(spent);
        sleep(remains).await;
    }
}

async fn run_periodic_loop(periodic: &Logger) -> Result<(), Box<dyn Error>> {
    let delay = periodic_delay();
    info!(
        "Running periodic loop with delay {:.3}s",
        delay.as_secs_f32()
    );
    loop {
        let period = Period::start(delay);
        sd_notify::ping_watchdog().await?;
        // Run periodic background tasks
        periodic
            .periodic()
            .await
            .inspect_err(|e| error!("Failed to perform periodic task: {:?}", e))?;
        period.sleep().await;
    }
}

async fn run_ds_clean_loop(ds: &Datastore) -> Result<(), Box<dyn Error>> {
    let clean_prime = Duration::from_millis(40487);
    loop {
        sleep(clean_prime).await;
        let pool = ds.pool();
        let clean = Datastore::clean_maintenance(pool)
            .await
            .inspect_err(|e| error!("Failed to perform cleanup operation: {:?}", e))?;
        if clean.trans_failed > 0 || clean.trans_old > 0 || clean.data_deleted > 0 {
            info!("Periodic db cleaned: {clean}");
        }
    }
}

#[async_std::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Parse commandline options
    let opt: Config = argh::from_env();

    // Set up logging, (tracing)
    {
        let filter = get_log_filter(opt.verbose);
        tracing_subscriber::registry()
            .with(fmt::layer())
            .with(filter)
            .init();
    };

    info!("Configuration: {:#?}", opt);
    info!("Version: {:?}", VERSION);

    let connection = make_connection(opt.session).await?;
    let pool = make_pool(opt.session, opt.db_connections, opt.db_timeout).await?;

    let ds = Datastore::new(pool).await?;

    let modio_submit1 = Submit1::builder()
        .datastore(ds.clone())
        .development(opt.session)
        .customer(false)
        .build()
        .await?;

    let cust_submit1 = Submit1::builder()
        .datastore(ds.clone())
        .development(opt.session)
        .customer(true)
        .build()
        .await?;

    let logger1 = Logger1::builder()
        .datastore(ds.clone())
        .development(opt.session)
        .build()
        .await?;

    let ds_periodic = ds.clone();
    let logger = Logger::new(timefail::Timefail::session(opt.session), ds).await?;
    let periodic = logger.clone();

    let loggerp = LoggerPing {};

    connection.object_server().at(LOGGER_PATH, logger).await?;
    connection.object_server().at(LOGGER_PATH, loggerp).await?;
    connection.object_server().at(LOGGER_PATH, logger1).await?;
    connection
        .object_server()
        .at("/se/modio/logger/modio", modio_submit1)
        .await?;
    connection
        .object_server()
        .at("/se/modio/logger/customer", cust_submit1)
        .await?;
    connection.request_name(LOGGER_NAME).await?;
    sd_notify::sd_ready().await?;
    let period_loop = run_periodic_loop(&periodic);
    let clean_loop = run_ds_clean_loop(&ds_periodic);
    // Wait for any of the loops to complete, if that happens it is an error and we should abort
    futures::try_join!(period_loop, clean_loop)?;
    Ok(())
}
