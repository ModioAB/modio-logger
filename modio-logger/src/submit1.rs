// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
mod builder;
use crate::logger::LogErr;
use crate::timefail::Timefail;
use crate::types::Metadata;
pub use builder::Builder;
use modio_logger_db::Datastore;
use tracing::{debug, error, info, instrument};
use zbus::interface;

pub struct Submit1 {
    customer: bool,
    ds: Datastore,
    _timefail: Timefail,
}

impl Submit1 {
    pub async fn new(timefail: Timefail, ds: Datastore, customer: bool) -> Result<Self, LogErr> {
        if timefail.is_timefail() {
            info!("Failing all pending change requests due to TIMEFAIL");
            ds.transaction_fail_pending().await?;
        }
        Ok(Self {
            ds,
            _timefail: timefail,
            customer,
        })
    }

    #[must_use]
    pub const fn builder() -> Builder {
        Builder::new()
    }
}

#[interface(name = "se.modio.logger.Submit1")]
impl Submit1 {
    /// Returns a vector of all metadata objects.
    #[instrument(skip_all)]
    pub(crate) async fn get_all_metadata(&mut self) -> Result<Vec<Metadata>, LogErr> {
        use std::convert::Into;
        let data = if self.customer {
            debug!("Retrieving all customer metadata");
            self.ds
                .get_all_metadata()
                .await
                .inspect_err(|e| error!("Failed to get all customer metadata: err={:?}", e))?
        } else {
            debug!("Retrieving all internal metadata");
            self.ds
                .get_all_internal_metadata()
                .await
                .inspect_err(|e| error!("Failed to get all internal metadata: err={:?}", e))?
        };
        let arr: Vec<Metadata> = data.into_iter().map(Into::into).collect();
        debug!("Returning {} metadata items", arr.len());
        Ok(arr)
    }
}

#[cfg(test)]
mod test {
    use crate::submit1::Submit1;
    use modio_logger_db::Datastore;
    use std::error::Error;
    use test_log::test;
    use timeout_macro::timeouttest;

    #[test(timeouttest)]
    async fn test_submit1_interface() -> Result<(), Box<dyn Error>> {
        use zbus::Interface;
        let submit1 = Submit1::builder().development(true).build().await?;
        let mut result = String::new();
        submit1.introspect_to_writer(&mut result, 0);
        assert_eq!(
            result,
            r#"<interface name="se.modio.logger.Submit1">
  <!--
   Returns a vector of all metadata objects.
   -->
  <method name="GetAllMetadata">
    <arg type="aa{sv}" direction="out"/>
  </method>
</interface>
"#
        );
        Ok(())
    }

    #[test(timeouttest)]
    async fn metadata_filters_for_modio() -> Result<(), Box<dyn Error>> {
        let ds = Datastore::temporary().await;
        ds.metadata_set_name("modio.key.key", "Internal only")
            .await?;
        ds.metadata_set_name("key.key.key.one", "Some Test Name")
            .await?;
        ds.metadata_set_name("key.key.key.two", "Some Test Name")
            .await?;
        ds.metadata_set_name("key.key.key.three", "Some Test Name")
            .await?;

        let mut submit1 = Submit1::builder()
            .development(true)
            .datastore(ds)
            .customer(false)
            .build()
            .await?;

        let res = submit1.get_all_metadata().await?;
        // Should have 1 key
        assert_eq!(res.len(), 1);
        assert!(res[0].n.starts_with("modio."));
        Ok(())
    }

    #[test(timeouttest)]
    async fn metadata_filters_on_customer() -> Result<(), Box<dyn Error>> {
        let ds = Datastore::temporary().await;
        ds.metadata_set_name("modio.key.key", "Internal only")
            .await?;
        ds.metadata_set_name("key.key.key.one", "Some Test Name")
            .await?;
        ds.metadata_set_name("key.key.key.two", "Some Test Name")
            .await?;
        ds.metadata_set_name("key.key.key.three", "Some Test Name")
            .await?;

        let mut submit1 = Submit1::builder()
            .development(true)
            .datastore(ds)
            .customer(true)
            .build()
            .await?;

        let res = submit1.get_all_metadata().await?;
        // should have "key.key...." but not "modio...."
        assert_eq!(res.len(), 3);
        assert!(res[0].n.starts_with("key.key.key"));
        Ok(())
    }
}
