// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use crate::types::DataTypeError;
use serde_json::Value as JsonValue;
use tracing::warn;
use zbus::zvariant::Value as zValue;

pub(crate) const fn valid_metric(value: &zValue<'_>) -> Result<(), DataTypeError> {
    use zValue::{Bool, Str, F64, I16, I32, I64, U16, U32, U64, U8};
    match value {
        U64(_) | U32(_) | U16(_) | U8(_) | F64(_) | Str(_) | Bool(_) | I16(_) | I32(_) | I64(_) => {
            Ok(())
        }
        _ => Err(DataTypeError::Unknown),
    }
}

pub(crate) type TimestampRow<'a> = (f64, Vec<zValue<'a>>);

/// Helper function for the data-validation of rows
pub(crate) fn check_data_row(
    expected: &Vec<modio_logger_db::DataType>,
    row: &Vec<zValue<'_>>,
) -> Result<(), DataTypeError> {
    // We could check all the values, but that will be done when converting them to db values, so
    // we skip it here.
    //
    // If we don't have the same amount of items in the row as we  should have, fail it.
    if row.len() != expected.len() {
        warn!(
            "Mismatching amount of values in row. expected={:?}  row={:?}",
            expected, row
        );
        return Err(DataTypeError::RowCount);
    }
    Ok(())
}

pub(crate) fn value_to_string(value: &zValue<'_>) -> Option<String> {
    use zValue::{Bool, Str, F64, I16, I32, I64, U16, U32, U64, U8};
    let res = match value {
        U64(v) => format!("{v}"),
        U32(v) => format!("{v}"),
        U16(v) => format!("{v}"),
        U8(v) => format!("{v}"),
        F64(v) => format!("{v}"),
        Str(v) => format!("{v}"),
        Bool(v) => format!("{}", u32::from(*v)),
        // Signed ints will be round-tripped as Floats, with precision loss.
        // But here, we can just call str() on them
        I16(v) => format!("{v}"),
        I32(v) => format!("{v}"),
        I64(v) => format!("{v}"),
        _ => return None,
    };
    Some(res)
}

pub(crate) fn value_to_json(value: zValue<'_>) -> Result<JsonValue, DataTypeError> {
    use zValue::{Bool, Str, F64, I16, I32, I64, U16, U32, U64, U8};
    let res = match value {
        U64(v) => JsonValue::from(v),
        U32(v) => JsonValue::from(v),
        U16(v) => JsonValue::from(v),
        U8(v) => JsonValue::from(v),
        I16(v) => JsonValue::from(v),
        I32(v) => JsonValue::from(v),
        I64(v) => JsonValue::from(v),
        F64(v) => JsonValue::from(v),
        Str(v) => JsonValue::from(v.to_string()),
        Bool(v) => JsonValue::from(v),
        _ => return Err(DataTypeError::Unknown),
    };
    Ok(res)
}

// Convert zbus vec of data to serde_json vec of data.
pub(crate) fn zbus_row_to_json_row(
    values: Vec<zValue<'_>>,
) -> Result<Vec<JsonValue>, DataTypeError> {
    values.into_iter().map(value_to_json).collect()
}

// This probably should not be done as JSON conversion here, but in the logger db layer where we
// can convert things to JSONB  or other data-types that are not strings.
pub(crate) fn zbus_row_to_json_string(values: Vec<zValue<'_>>) -> Result<String, DataTypeError> {
    let jsoned = zbus_row_to_json_row(values)?;
    let value = serde_json::to_string(&jsoned)?;
    Ok(value)
}
