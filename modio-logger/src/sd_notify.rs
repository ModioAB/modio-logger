// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use async_std::os::unix::net::UnixDatagram;
use std::env;
use std::io::{self, ErrorKind};
use std::str::FromStr;
use std::time::Duration;

/// Pick up the socket path to notify and then send the message to it.
/// Details on the protocol are here <https://www.man7.org/linux/man-pages/man3/sd_notify.3.html>
pub async fn sd_notify(msg: &str) -> io::Result<()> {
    if let Some(socket_path) = env::var_os("NOTIFY_SOCKET") {
        let sock = UnixDatagram::unbound()?;
        let len = sock.send_to(msg.as_bytes(), socket_path).await?;
        if len == msg.len() {
            Ok(())
        } else {
            let err = io::Error::new(ErrorKind::WriteZero, "incomplete write");
            Err(err)
        }
    } else {
        Ok(())
    }
}

/// Helper, send "READY=1"
pub async fn sd_ready() -> io::Result<()> {
    sd_notify("READY=1").await
}

/// Helper, send "WATCHDOG=1"
pub async fn ping_watchdog() -> io::Result<()> {
    sd_notify("WATCHDOG=1").await
}

/// Read the env and get the watchdog usec
pub fn sd_watchdog_usec() -> Option<Duration> {
    let usec_raw = env::var_os("WATCHDOG_USEC")?;
    // OsStrings can contain data that are not valid &str, convert here.
    let usec_str = usec_raw.to_str()?;
    // Try to parse the string, convert Err to None and bubble up.
    let usec = u64::from_str(usec_str).ok()?;
    let dur = Duration::from_micros(usec);
    Some(dur)
}
