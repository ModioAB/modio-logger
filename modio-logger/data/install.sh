cp dbus/se.modio.logger.conf	/etc/dbus-1/system.d/se.modio.logger.conf
cp dbus/se.modio.logger.service /usr/local/share/dbus-1/system-services/se.modio.logger.service
cp systemd/mocklogger.service	/usr/local/lib/systemd/system/mocklogger.service

systemctl stop mocklogger
systemctl daemon-reload
systemctl reload dbus
systemctl enable mocklogger
