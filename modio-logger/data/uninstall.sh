rm /etc/dbus-1/system.d/se.modio.logger.conf
rm /usr/local/share/dbus-1/system-services/se.modio.logger.service
rm /usr/local/lib/systemd/system/mocklogger.service

systemctl stop mocklogger
systemctl daemon-reload
systemctl reload dbus
