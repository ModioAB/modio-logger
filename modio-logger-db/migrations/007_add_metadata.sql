-- tags, simpe unique tags for a sensor
CREATE TABLE IF NOT EXISTS tag_single (
	tag_id INTEGER PRIMARY KEY,
	s_id REFERENCES sensor ON DELETE CASCADE,
	tag VARCHAR NOT NULL  CHECK (tag in ("unit", "name", "description", "enum")),
	value VARCHAR NOT NULL,
	UNIQUE(s_id, tag)
);

PRAGMA user_version=7;
