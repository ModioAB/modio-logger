-- change logdata timestamp to float
CREATE TABLE new_logdata (
	id INTEGER PRIMARY KEY,
        status VARCHAR NOT NULL DEFAULT 'NONE' CHECK (
		status IN ('NONE', 'REMOVED', 'TIMEFAIL')
	),
	s_id INTEGER NOT NULL REFERENCES sensor,
        value VARCHAR NOT NULL,
        time FLOAT NOT NULL
);
INSERT INTO new_logdata (id, status, s_id, value, time) SELECT id, status, s_id, value, time FROM logdata;
DROP TABLE logdata;
ALTER TABLE new_logdata RENAME TO logdata;
CREATE INDEX logdata_s_id ON logdata(s_id);
PRAGMA user_version=12;
