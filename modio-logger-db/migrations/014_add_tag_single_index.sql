-- add the tag_single s_id index
CREATE INDEX IF NOT EXISTS tag_single_s_id ON tag_single(s_id);
PRAGMA user_version=10;
