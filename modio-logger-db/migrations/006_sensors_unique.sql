DELETE FROM sensor WHERE rowid NOT IN (SELECT MIN(rowid) FROM sensor GROUP BY sensor.name);
DELETE FROM logdata WHERE s_id NOT IN (SELECT s_id FROM sensor);
DELETE FROM changes WHERE s_id NOT IN (SELECT s_id FROM sensor);
CREATE UNIQUE INDEX uq_sensor_name ON sensor(name);
pragma user_version=6;
