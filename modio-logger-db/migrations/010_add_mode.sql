-- add tag for "mode"

ALTER table tag_single RENAME TO tag_single_old;
CREATE TABLE IF NOT EXISTS tag_single (
	tag_id INTEGER PRIMARY KEY,
	s_id REFERENCES sensor ON DELETE CASCADE,
	tag VARCHAR NOT NULL  CHECK (tag in ("unit", "name", "description", "enum", "mode")),
	value VARCHAR NOT NULL,
	UNIQUE(s_id, tag)
);
INSERT INTO tag_single (tag_id, s_id, tag, value) SELECT tag_id, s_id, tag, value FROM tag_single_old;
DROP TABLE tag_single_old;
PRAGMA user_version=10;
