CREATE TABLE IF NOT EXISTS sensor (
	s_id integer primary key,
	name varchar not null
);

INSERT OR IGNORE INTO sensor(s_id, name) VALUES (1, 'mytemp.internal.sensors');

CREATE TABLE IF NOT EXISTS logdata (
	id integer primary key,
        status varchar not null default 'NONE' check (
		status in ('NONE', 'REMOVED', 'TIMEFAIL')
	),
	s_id integer not null references sensor,
        value varchar not null,
        time integer not null
);


CREATE TABLE IF NOT EXISTS changes (
	t_id integer primary key,
	status varchar not null default 'NONE' check  (status in ('NONE', 'REMOVED', 'SUCCESS', 'FAILED', 'PENDING')),
	s_id integer not null references sensor,
	token integer not null,
	expected varchar not null,
	target varchar not null,
	UNIQUE(s_id, token)
);

pragma user_version=5;
