-- add the logdata s_id index
CREATE INDEX IF NOT EXISTS logdata_s_id ON logdata(s_id);
PRAGMA user_version=9;
