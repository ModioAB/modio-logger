mod ds;
mod raw;
pub use super::db::SqlitePool;
pub use crate::buffer::{fxtime, fxtime_ms, inixtime, unixtime};
pub use ds::Datastore;

#[derive(Debug, Copy, Clone)]
enum TransactionStatus {
    Success,
    Failed,
}

impl TransactionStatus {
    const fn as_str(&self) -> &str {
        match self {
            Self::Success => "SUCCESS",
            Self::Failed => "FAILED",
        }
    }
    const fn as_logval(&self) -> &str {
        match self {
            Self::Success => "1",
            Self::Failed => "0",
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum TagType {
    Name,
    Unit,
    Description,
    Enum,
    Mode,
    Row,
}
impl TagType {
    const fn as_str(&self) -> &str {
        match self {
            Self::Name => "name",
            Self::Unit => "unit",
            Self::Description => "description",
            Self::Enum => "enum",
            Self::Mode => "mode",
            Self::Row => "row",
        }
    }
}
