// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("SQL Reported error")]
    SQLx { source: sqlx::Error },
    #[error("Migration error")]
    Migration {
        #[from]
        source: sqlx::migrate::MigrateError,
    },
    #[error("Unique constraint failed")]
    Unique { source: sqlx::Error },
    #[error("Database Pool Timeout")]
    Timeout { source: sqlx::Error },
    #[error("Not found")]
    NotFound { source: sqlx::Error },
    #[error("Serialization failed")]
    Serde(#[from] serde_json::Error),
}

impl std::convert::From<sqlx::Error> for Error {
    fn from(source: sqlx::Error) -> Self {
        match source {
            sqlx::Error::RowNotFound => Self::NotFound { source },
            sqlx::Error::PoolTimedOut => Self::Timeout { source },
            _ => {
                if source.to_string().contains("UNIQUE constraint failed") {
                    Self::Unique { source }
                } else {
                    Self::SQLx { source }
                }
            }
        }
    }
}
